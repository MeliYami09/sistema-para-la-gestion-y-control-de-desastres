const { Router } = require('express');
const router = new Router();
const _ = require('underscore');

const donamedicina = require('../sample.json');

router.get('/', (req, res) => {
    res.json(donamedicina);
});

router.post('/', (req, res) => {
    const id = donamedicina.length + 1;
    const { nombre, dosis, año, precio } = req.body;
    const nuevadonaMedicina = { ...req.body, id };
    if (id && nombre && dosis && año && precio) {
        donamedicina.push(nuevadonaMedicina);
        res.json(donamedicina);
    } else {
        res.status(500).json({error: 'ah ocurrido un error.'});
    }
});

router.put('/:id', (req, res) => {
    const { id } = req.params;
    const { nombre, dosis, año, precio } = req.body;
    if (id && nombre && dosis && año && precio) {
        _.each(donamedicina, (medicina, i) => {
            if (medicina.id === id) {
                medicina.nombre = nombre;
                medicina.dosis = dosis;
                medicina.año = año;
                medicina.precio = precio;
            }
        });
        res.json(donamedicina);
    } else {
        res.status(500).json({error: 'tuvistes un error'});
    }
});

router.delete('/:id', (req, res) => {
    const {id} = req.params;
    if (id) {
        _.each(donamedicina, (medicina, i) => {
            if (medicina.id == id) {
                donamedicina.splice(i, 1);
            }
        });
        res.json(donamedicina);
    }
});

module.exports = router;