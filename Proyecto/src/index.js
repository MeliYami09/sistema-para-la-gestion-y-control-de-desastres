const express = require('express');
const morgan = require('morgan');
const app = express();

// settings
app.set('port', process.env.PORT || 4000);

// middlewares
app.use(morgan('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

// routes
app.use(require('./routes'));
app.use('/api/donamedicina', require('./routes/donamedicina'));


// starting the server
app.listen(app.get('port'), () => {
    console.log(`EL SERVIDOR ESTA ABIERTO ${app.get('port')}`);
});
